﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquipagePirate
{
    class Bateau
    {
        private  String nom;
        private int nbEquipage;
        private Pirate capitaine;
        private List<Pirate> equipage;
        private bool etat;

        public string Nom { get => nom; set => nom = value; }
        public int NbEquipage { get => nbEquipage; set => nbEquipage = value; }
        internal Pirate Capitaine { get => capitaine; set => capitaine = value; }
        internal List<Pirate> Equipage { get => equipage; }
        public bool Etat { get => etat; set => etat = value; }

        public String PasserALaPlanche( Pirate pirate)
        {
            if (Equipage.Contains(pirate))
            {
                pirate.Etat = false;
                Equipage.Remove(pirate);
            }
            return "plouf";
        }

        public String Aborder(Bateau cible)
        {
            foreach (Pirate pirate in cible.Equipage)
            {
                pirate.Etat = false;
                Console.WriteLine(pirate.Nom + " " + pirate.Prenom + " repose au fond de la mer ");
                cible.Equipage.Remove(pirate);
            }

            cible.Etat = false;
            return (cible.Nom + " a été pillé");

        }

        public void Embarquer ( List<Pirate> listPirate)
        {
            foreach ( Pirate p in listPirate)
            {
                Embarquer(p);
            }
        }

        public String Embarquer( Pirate pirate)
        {
            if (NbEquipage < Equipage.Count + 1)
            {
                if (Capitaine != null)
                {
                    Equipage.Add(pirate);
                    return pirate.Nom + " A embarqué sur le " + Nom;
                }
                else
                {
                    Capitaine = pirate;
                    return Capitaine.Nom + " est maintenant le capitaine du " + Nom;
                }
            }
            else return " le " + Nom + " ne prend plus de membre d'équipage";

        }

        public Bateau( String nom)
        {
            equipage = new List<Pirate>();
            Nom = nom;
        }

        public Bateau(String nom, Pirate pirate)
        {
            equipage = new List<Pirate>();
            Nom = nom;
            Capitaine = pirate;
        }

    }
}
