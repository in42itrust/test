﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquipagePirate
{
    class BateauV2
    {
       private  String nom;
       private  int nbEquipage;
       private  Pirate capitaine;
       private bool etat;



        private List<Pirate> matelots = new List<Pirate>();
        private List<Pirate> gabiers = new List<Pirate>();
        private List<Pirate> moucheurs = new List<Pirate>();
        private Dictionary<string, List<Pirate>> Equipage = 
            new Dictionary<string, List<Pirate>>();


        public string Nom { get => nom; set => nom = value; }
        public int NbEquipage { get => nbEquipage; set => nbEquipage = value; }
        public bool Etat { get => etat; set => etat = value; }
        internal Pirate Capitaine { get => capitaine; set => capitaine = value; }

        internal List<Pirate> Matelots { get => matelots; set => matelots = value; }
        internal List<Pirate> Gabiers { get => gabiers; set => gabiers = value; }
        internal List<Pirate> Moucheurs { get => moucheurs; set => moucheurs = value; }
       
        internal Dictionary<string, List<Pirate>> Equipage1 { get => Equipage; set => Equipage = value; }



        Dictionary<String, List<Pirate>> equipage;

        public BateauV2(string nom, int nbEquipage, Pirate capitaine)
        {
            this.nom = nom;
            this.nbEquipage = nbEquipage;
            this.capitaine = capitaine;
            Matelots = new List<Pirate>();
            Gabiers = new List<Pirate>();
            Moucheurs = new List<Pirate>();
            Equipage = new Dictionary<string, List<Pirate>>();
            Equipage.Add("matelots", Matelots);
            Equipage.Add("gabiers", Gabiers);
            Equipage.Add("moucheurs", Moucheurs);
        }

        public String PasserALaPlanche(Pirate pirate)
        {
            foreach (List<Pirate> poste in equipage.Values)
            {
                if (poste.Contains(pirate))
                {
                    pirate.etat = false;
                    poste.Remove(pirate);
                    return "plouf";
                }
            }

            return pirate.nom + " " + pirate.prenom + " n'est pas un membre d'équipage.";
        }


    



        public string PromouvoirCapitain(Pirate piratePromu)
        {

            if (Capitaine == null)
            {

                foreach (List<Pirate> poste in equipage.Values)
                    if (poste.Contains(piratePromu))
                    {

                        Capitaine = piratePromu;
                        poste.Remove(piratePromu);
                        return " le " + Nom + " est mantenant dirigé par " + Capitaine.nom + " " + Capitaine.prenom;


                    }
                    else return Capitaine.nom + " " + Capitaine.prenom + " n'est pas un membre de l'équipage du " + Nom;

            }
            else return " le " + Nom + " a déja un capitaine . Il d'agit de " + Capitaine.nom + " " + Capitaine.prenom;


            return "";
        }








    }
}
