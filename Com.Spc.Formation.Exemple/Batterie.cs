﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Com.Spc.Formation.Exemple
{
    class Batterie
    {

        private int pourcentage = 100;

        public int Pourcentage 
        {
            get => pourcentage;
            set => pourcentage = value;
        }

        public int utilsationBaterie( int puissance)
        {

             if ( Pourcentage - puissance > 0)
            {
               return  Pourcentage -= puissance;
            }
            else
            {
                throw new EnergieExeption(" la batterie est vide ");
            }

        }
    }
}
