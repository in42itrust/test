﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.spc.Formation.Personne
{
    class Program
    {
        static void Main(string[] args)
        {
            Personne personne1 = new Personne();
           
            Personne personne2 = new Personne("Sparrow", "Jack", 42);
            Console.WriteLine(personne1.sePresenter());
            Console.WriteLine(personne2.sePresenter());
            Console.WriteLine(personne2.sePresenter(true));
            Console.WriteLine(personne2.Nom);
            personne1.Nom = "Teach";
            personne1.Prenom = "Edward";
            personne1.Age = 54;
            Console.WriteLine(personne1.sePresenter());
            Console.ReadLine();


        }
    }
}
