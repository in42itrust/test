﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PirateBdd
{
    class Program
    {
        static void Main(string[] args)
        {

            using (PiraterieEntities1 context = new PiraterieEntities1())
            {
                // recuperer la liste des Bateaux 
                var query = from b in context.Bateau
                                // join ( p => p.Pirate.Id == b.Capitaine && p == context.Pirate ),
                                join pirate in context.Pirate on b.Capitaine equals pirate.Id
                                where b.Capitaine == pirate.Id
                                 select new { Bateau = b , pirate };

                foreach ( var entry in query)
                {
                   

                   

                    Console.WriteLine(" le bateau {0} a pour capitaine {1}", entry.Bateau.Nom , entry.pirate.Nom );
                }


                Pirate john = new Pirate { Nom = "Silver", Prenom = "Long John", vivant = true, Id = 42  };
                context.Pirate.Add(john);
                context.SaveChanges();

                Console.WriteLine(john.Id);

                Console.WriteLine( GestionEquipage.ajouterAUnEquipage(context, 1, john));

                GestionEquipage.TuerCapitaine(context, 1 );

                foreach ( Pirate pirate in GestionEquipage.InMemoriam(context))
                {
                    Console.WriteLine("repose en paix " + pirate.Nom + " " + pirate.Prenom);
                }

                Console.ReadKey();
            }

        }
    }
}
