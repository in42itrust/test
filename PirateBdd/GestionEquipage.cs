﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PirateBdd
{
    class GestionEquipage
    {
        

        internal static void TuerCapitaine(PiraterieEntities1 context, int bateauID )
        {
            var bateau = (from b in context.Bateau
                         where b.Id == bateauID
                         select b).FirstOrDefault();

            var deadCaptain = (from p in context.Pirate
                               where p.Id == bateau.Capitaine
                               select p).FirstOrDefault();


            bateau.Capitaine = null;
            deadCaptain.vivant = false;

            context.SaveChanges();

            
        }

        internal static List<Pirate> InMemoriam(PiraterieEntities1 context)
        {



            List<Pirate> deadPirates = (from p in context.Pirate
                                       where p.vivant == false
                                       select p).ToList();


            return deadPirates;
        }

        

        internal static String ajouterAUnEquipage(PiraterieEntities1 context, int BateauID, Pirate recrue)
        {
            String resultat = "";
            var marins = from e in context.Equipage
                         where e.Bateau == BateauID
                         select e;
            var bateau = (from c in context.Bateau
                           where c.Id == BateauID
                           select c).FirstOrDefault();



            if (marins.Count() < bateau.Capacite - 1)
            {
                Equipage ajout = new Equipage { Bateau = BateauID, Pirate = recrue.Id, Id =  };
                resultat = " le pirate " + recrue.Nom + "fait maintenant parti de l'equipage du " + bateau.Nom;
            }
            else resultat = "l'equipage du " + bateau.Nom + " est complet";

            return resultat;

            

        }
    }
}
