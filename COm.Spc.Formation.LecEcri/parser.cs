﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COm.Spc.Formation.LecEcri
{
    class parser
    {


        public List<String[]> resultatsParams(String path)
        {
            List<String[]> resultats = new List<string[]>();

            String[] readed = ParseTotal(path);

            for (int i = 0; i < readed.Length; i++)
            {
                
                String[] pr = parseData(readed[i]);
                
                resultats.Add(pr);

            }

            return resultats;
        }




        public String[] ParseTotal( String path)
        {
           
            return File.ReadAllLines(path);
        }

        public String[] parseData(String result)
        {
            String[] resultParam = result.Split(';');
            for ( int s = 0; s < resultParam.Length; s++)
            {
                resultParam[s] = resultParam[s].Substring(1, resultParam[s].Length - 2);
            }
            return resultParam;

        }

        


    }
}
